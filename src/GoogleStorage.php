<?php namespace juliorafaelr\GoogleStorage;

use Exception;
use Google\Cloud\Storage\StorageClient;

class GoogleStorage {
    protected $storage;

    protected $config;

    function __construct( $keyFilePath ) {
        $this->config = [
            'keyFilePath' => $keyFilePath,
        ];

        $this->storage = new StorageClient( $this->config );
    }

	/**
	 * @param $bucketName
	 * @param $objectName
	 * @param $destination
	 *
	 * @throws Exception
	 */

	public function downloadObject( $bucketName, $objectName, $destination ) {
		list( $object,$info ) = $this->getStorageObjectObject( array( 'bucketName' => $bucketName, 'objectName' => $objectName ) );

		if ( (int)$info[ 'size' ] === 0 ) {
			throw new Exception( 'file is empty. nothing to download' );
		}

		$object->downloadToFile( $destination );
	}

	/**
	 * Upload a file from the fileSystem to Google Storage
	 *
	 * @param string $bucketName
	 * @param string $objectName
	 * @param string $source
	 * @param bool $deleteSourceFile
	 *
	 * @return array
	 *
	 * @throws Exception
	 */

	function uploadObject( string $bucketName, string $objectName, string $source, bool $deleteSourceFile = true ): array {
		if ( is_file( $source ) ) {
			$file = fopen( $source, 'r' );
		}
		else {
			throw new Exception('file does not exist or is damage');
		}

		$bucket = $this->storage->bucket( $bucketName );

		$object = array( );

		if ( filesize( $source ) !== 0 ) {
			$object = $bucket->upload( $file, [
				'name' => $objectName
			] );
		}

		if ( $deleteSourceFile === true ) {
			usleep( 200000 );

			unlink( $source );
		}

		return $object->info( );
	}

	/**
	 * Upload a file from the fileSystem to Google Storage using CLI
	 *
	 * @param string $bucketName
	 * @param string $objectName
	 * @param string $source
	 * @param bool $deleteSourceFile
	 *
	 * @throws Exception
	 */

	function uploadObjectStream( string $bucketName, string $objectName, string $source, bool $deleteSourceFile = true ): void {
		if ( is_file( $source ) === false ) {
			throw new Exception( 'file does not exist or is damage' );
		}

		if ( filesize( $source ) !== 0 ) {
			exec( 'gsutil cp ' . $source . ' gs://' . $bucketName . '/' . $objectName, $output, $status );
		}


		if ( $deleteSourceFile === true ) {
			usleep( 200000 );

			unlink( $source );
		}
	}

	/**
	 * upload a list of file one by one to the storage
	 *
	 * @param string $bucketName
	 * @param string $objectName
	 * @param string $path
	 * @param string $prefix
	 * @param bool $deleteSourceFile
	 *
	 * @return array
	 *
	 * @throws Exception
	 */

	public function uploadListFilesStream( string $bucketName, string $objectName, string $path, string $prefix, bool $deleteSourceFile = true ) {
		$fileList = array( );

		foreach( glob( $path . '/' . $prefix . '*' ) as $f ) {
			$this->uploadObjectStream( $bucketName, $objectName, $f, $deleteSourceFile );

			$fileList[ ] = $f;
		}

		return $fileList;
	}

	/**
	 * Upload a group of files from the fileSystem to Google Storage using CLI
	 *
	 * @param string $bucketName
	 * @param string $objectName
	 * @param string $sourcePath
	 * @param string $sourcePrefix
	 *
	 * @return int
	 *
	 * @throws Exception
	 */

	function uploadMassiveStream( string $bucketName, ?string $objectName, string $sourcePath, string $sourcePrefix = '' ): int {
		if ( empty( $objectName ) === false ) {
			$objectName = $objectName . '/';
		}

		$exec_string = 'gsutil -m cp ' . '"' . $sourcePath . '/' . $sourcePrefix . '*"' . ' gs://' . $bucketName . '/' . $objectName;

		exec( $exec_string, $output, $status );

		return $status;
	}

	/**
	 * @param string $bucketName
	 * @param string $objectName
	 *
	 * @throws Exception
	 */

	public function deleteObject( string $bucketName, string $objectName ): void {
		if ( strpos( $objectName, '*' ) !== false ) {
			$name = explode( '*', $objectName );

			$objects = $this->listObjects( $bucketName, [ 'prefix' => $name ] );

			foreach ( $objects as $object ) {
				$this->__delete_object($bucketName, $object);
			}
		}
		else {
			$this->__delete_object($bucketName, $objectName);
		}
	}

	/**
	 * move one object to a new bucket (if specified) with a new name (if specified)
	 *
	 * @param string $bucketName
	 * @param string $objectName
	 * @param string|null $newBucketName
	 * @param string|null $newObjectName
	 *
	 * @throws Exception
	 */

	public function moveObject( string $bucketName, string $objectName, ?string $newBucketName =  null, ?string $newObjectName = null ): void {
		if ( stripos( $objectName, '*' ) !== false ) {
			$name = explode( '*', $objectName )[ 0 ];

			$objects = $this->listObjects( $bucketName, array( 'prefix' => $name ) );

			foreach ( $objects as $object ) {
				if ( empty( $newBucketName ) === true ) {
					$newBucketName = $bucketName;
				}

				if ( empty( $newObjectName ) === true ) {
					$newObjectName = $object;
				}
				else {
					$objectNameArray = explode( '/', $object );

					$name = end( $objectNameArray );

					$newObjectNameArray = explode( '/', $newObjectName );

					$newName = end( $newObjectNameArray );

					$objectWithoutName = str_replace( $newName,  '', $newObjectName );

					$newObjectName = $objectWithoutName . $name;
				}

				$this->__move_object( $bucketName, $object, $newBucketName, $newObjectName );
			}
		}
		else {
			if ( empty( $newBucketName ) === true ) {
				$newBucketName = $bucketName;
			}

			if ( empty( $newObjectName ) === true ) {
				$newObjectName = $objectName;
			}

			$this->__move_object( $bucketName, $objectName, $newBucketName, $newObjectName );
		}
	}

	/**
	 * move one object to a new bucket (if specified) with a new name (if specified)
	 *
	 * @param string $bucketName
	 * @param string $objectName
	 * @param string|null $newBucketName
	 * @param string|null $newObjectName
	 */

	public function moveObjectStreaming( string $bucketName, string $objectName, ?string $newBucketName =  null, ?string $newObjectName = null ): void {
		if ( stripos( $objectName, '*' ) !== false ) {
			$name = explode( '*', $objectName )[ 0 ];

			$objects = $this->listObjects( $bucketName, array( 'prefix' => $name ) );

			foreach ( $objects as $object ) {
				if ( empty( $newBucketName ) === true ) {
					$newBucketName = $bucketName;
				}

				if ( empty( $newObjectName ) === true ) {
					$newObjectName = $object;
				}
				else {
					$objectNameArray = explode( '/', $object );

					$name = end( $objectNameArray );

					$newObjectNameArray = explode( '/', $newObjectName );

					$newName = end( $newObjectNameArray );

					$objectWithoutName = str_replace( $newName,  '', $newObjectName );

					$newObjectName = $objectWithoutName . $name;
				}

				$this->__move_object_streaming( $bucketName, $object, $newBucketName, $newObjectName );
			}
		}
		else {
			if ( empty( $newBucketName ) === true ) {
				$newBucketName = $bucketName;
			}

			if ( empty( $newObjectName ) === true ) {
				$newObjectName = $objectName;
			}

			$this->__move_object_streaming( $bucketName, $objectName, $newBucketName, $newObjectName );
		}
	}

	public function getStorageClientObject() {
		return $this->storage;
	}

	/**
	 * @param $bucketName
	 * @param $objectName
	 * @param $destination
	 *
	 * @return mixed
	 *
	 * @throws Exception
	 */

	public function streamingDownload( $bucketName, $objectName, $destination ) {
		list( $object, $info ) = $this->getStorageObjectObject( [ 'bucketName' => $bucketName, 'objectName' => $objectName ] );

		unset( $object );

		if ( (int)$info[ 'size' ] === 0 ) {
			throw new Exception ( 'file is empty. nothing to download' );
		}

		exec("gsutil cp gs://$bucketName/$objectName - > $destination",$output,$status);

		return $status;
	}

	/**
	 * @param $bucketName
	 * @param $objectName
	 * @return mixed
	 * @throws Exception
	 */

	public function getObjectInfo( $bucketName, $objectName ) {
		list( $object, $info ) = $this->getStorageObjectObject( [ 'bucketName' => $bucketName, 'objectName' => $objectName ] );

		unset( $object );

		$info[ 'size_human' ] = $this->humanFileSize($info['size']);

		return $info;
	}

	/**
	 * @param $bucketName
	 *
	 * @return array
	 */

	public function getBucketInfo( $bucketName ) {
		$info = [];
		exec("gsutil du -s gs://$bucketName",$output);
		$result = explode(" ", $output[0]);
		$info['size'] = $result[0];
		$info['name'] = end($result);
		$info['size_human'] = $this->humanFileSize($info['size']);
		unset($output);

		exec("gsutil ls -L -b gs://$bucketName",$output);

		$result = explode(":", $output[1]);
		$info['storage_class'] = trim(end($result));

		$result = explode(":", $output[2]);
		$info['location_constraint'] = trim(end($result));

		$result = explode(":", $output[3]);
		$info['versioning_enabled'] = trim(end($result));

		$result = explode(":", $output[4]);
		$info['logging_configuration'] = trim(end($result));

		$result = explode(":", $output[5]);
		$info['website_configuration'] = trim(end($result));

		$result = explode(":", $output[6]);
		$info['cors_configuration'] = trim(end($result));

		$result = explode(":", $output[7]);
		$info['lifecycle_configuration'] = trim(end($result));

		$result = explode(":", $output[8]);
		$info['requester_pays_enabled'] = trim(end($result));

		$result = explode(":", $output[9]);
		$info['labels'] = trim(end($result));

		$result = explode("|", preg_replace('/:/', '|', $output[10], 1));
		$info['time_created'] = trim(end($result));

		$result = explode("|", preg_replace('/:/', '|', $output[11], 1));
		$info['time_updated'] = trim(end($result));

		return $info;
	}

	/**
	 * @param $bucketData
	 *
	 * @return array
	 *
	 * @throws Exception
	 */

	public function getStorageObjectObject( $bucketData ) {
		$storage = $this->storage;

		$object = $storage->bucket($bucketData['bucketName'])->object($bucketData['objectName']);

		$info = $object->info( );

		return array( $object,$info );
	}

	/**
	 * @param $bucketName
	 *
	 * @return mixed
	 */

	public function listObjectsStream( $bucketName ) {
		exec( "gsutil ls gs://$bucketName",$output );

		return $output;
	}

	/**
	 * retrieve list of objects inside an object, it can be passed a prefix to narrow the search.
	 *
	 * ```
	 * $options = array( 'prefix' => $name );
	 * ```
	 *
	 * @param string $bucketName
	 * @param array $options
	 *
	 * @return array
	 */

	public function listObjects( string $bucketName, $options = array( ) ): array {
		$objects = array( );

		$bucket = $this->storage->bucket( $bucketName );

		foreach ( $bucket->objects( $options ) as $object ) {
			$objects[ ] = $object->name( );
		}

		return $objects;
	}

	/**
	 * @param string $bucketName
	 * @param array $options
	 *
	 * @return bool
	 */

	public function hasObjects( string $bucketName, array $options = array( ) ): bool {
		unset( $options );

		return count($this->listObjectsStream($bucketName)) > 0;
	}

	/**
	 * @param $bucketName
	 * @param $objectName
	 * @param $newBucketName
	 * @param $newObjectName
	 * @throws Exception
	 */

	private function __move_object( $bucketName, $objectName, $newBucketName, $newObjectName ) {
		list($object,$info) = $this->getStorageObjectObject( array( 'bucketName' => $bucketName, 'objectName' => $objectName ) );

		unset( $info );

		$object->copy( $newBucketName, array( 'name' => $newObjectName ) );

		$object->delete( );
	}

	/**
	 * @param $bucketName
	 * @param $objectName
	 * @param $newBucketName
	 * @param $newObjectName
	 */

	private function __move_object_streaming( $bucketName, $objectName, $newBucketName, $newObjectName ) {
		$cmd_mv_file = 'gsutil -m mv gs://' . $bucketName . '/' . $objectName . ' ' . 'gs://' . $newBucketName . '/' . $newObjectName;

		exec( $cmd_mv_file, $output, $return );
	}

	/**
	 * @param $bucketName
	 *
	 * @param $objectName
	 *
	 * @throws Exception
	 */

	private function __delete_object( $bucketName, $objectName ) {
		list( $object, $info ) = $this->getStorageObjectObject( array( 'bucketName' => $bucketName, 'objectName' => $objectName ) );

		unset( $info );

		$object->delete( );
	}

	/**
	 * @param int $size
	 *
	 * @param string $unit
	 *
	 * @return string
	 */

	private function humanFileSize( int $size, string $unit = '' ): string {
		if ( ( !$unit && $size >= 1 << 40 ) || $unit === 'TB' ) {
			return number_format( $size / ( 1 << 40 ), 2 ) . 'TB';
		}
		if ( ( !$unit && $size >= 1 << 30 ) || $unit === 'GB' ) {
			return number_format( $size / ( 1 << 30 ), 2 ) . 'GB';
		}
		if ( ( !$unit && $size >= 1 << 20 ) || $unit === 'MB' ) {
			return number_format( $size / ( 1 << 20 ), 2 ) . 'MB';
		}
		if ( ( !$unit && $size >= 1 << 10 ) || $unit === 'KB' ) {
			return number_format( $size / ( 1 << 10 ), 2 ) . 'KB';
		}

		return number_format( $size ) . ' bytes';
	}
}
